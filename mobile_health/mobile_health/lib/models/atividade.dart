class Atividade{

  int _id;
  int _idUsuario;
  String _tituloAtividade;
  String  _exercicio;
  double _duracao;
  double _distancia;
  double _calorias;
  String _data;
  int _diaSemana;
  int _semanaAno;

  getId(){return _id;}
  getIdUsuario(){return _idUsuario;}
  getTituloAtividade(){return _tituloAtividade;}
  getExercicio(){return _exercicio;}
  getDuracao(){return _duracao;}
  getDistancia(){return _distancia;}
  getCalorias(){return _calorias;}
  getData(){return _data;}
  getDiaSemana(){return _diaSemana;}
  getSemanaAno(){return _semanaAno;}

  setId(int id){_id = id;}
  setIdUsuario(int idUsuario){_idUsuario = idUsuario;}
  setTituloAtividade(String tituloAtividade){_tituloAtividade = tituloAtividade;}
  setExercicio(String exercicio){_exercicio = exercicio;}
  setDuracao(double duracao){_duracao = duracao;}
  setDistancia(double distancia){_distancia = distancia;}
  setCalorias(double calorias){_calorias = calorias;}
  setData(String data){_data = data;}
  setDiaSemana(int diaSemana){_diaSemana = diaSemana;}
  setSemanaAno(int semanaAno){_semanaAno = semanaAno;}
}