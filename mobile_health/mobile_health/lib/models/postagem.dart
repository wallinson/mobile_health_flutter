class Postagem{

  int _id;
  int _idUsuario;
  String _textoPublicacao;
  String  _username;
  var _imagem;

  getId(){return _id;}
  getIdUsuario(){return _idUsuario;}
  getTextoPublicacao(){return _textoPublicacao;}
  getUsername(){return _username;}
  getImagem(){return _imagem;}

  setId(int id){_id = id;}
  setIdUsuario(int idUsuario){_idUsuario = idUsuario;}
  setTextoPublicacao(String textoPublicacao){_textoPublicacao = textoPublicacao;}
  setUsername(String username){_username = username;}
  setImagem(var imagem){_imagem = imagem;}

}