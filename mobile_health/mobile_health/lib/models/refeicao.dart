class Refeicao{

  int _id;
  int _idUsuario;
  String _tipoRefeicao;
  String  _alimento;
  double _quantidade;
  String _medida;
  double _caloria;
  String _data;
  int _diaSemana;
  int _semanaAno;

  getId(){return _id;}
  getIdUsuario(){return _idUsuario;}
  getTipoRefeicao(){return _tipoRefeicao;}
  getAlimento(){return _alimento;}
  getQuantidade(){return _quantidade;}
  getMedida(){return _medida;}
  getCaloria(){return _caloria;}
  getData(){return _data;}
  getDiaSemana(){return _diaSemana;}
  getSemanaAno(){return _semanaAno;}


  setId(int id){_id = id;}
  setIdUsuario(int idUsuario){_idUsuario = idUsuario;}
  setTipoRefeicao(String tipoRefeicao){_tipoRefeicao = tipoRefeicao;}
  setAlimento(String alimento){_alimento = alimento;}
  setQuantidade(double quantidade){_quantidade = quantidade;}
  setMedida(String medida){_medida = medida;}
  setCaloria(double caloria){_caloria = caloria;}
  setData(String data){_data = data;}
  setDiaSemana(int diaSemana){_diaSemana = diaSemana;}
  setSemanaAno(int semanaAno){_semanaAno = semanaAno;}

}