class Usuario{

  int _id;
  String _nomeUsuario;
  String _email;
  String  _senha;
  int _altura;
  double _peso;
  String _username;
  var _imagemPerfil;

  getId(){return _id;}
  getNomeUsuario(){return _nomeUsuario;}
  getEmail(){return _email;}
  getSenha(){return _senha;}
  getAltura(){return _altura;}
  getPeso(){return _peso;}
  getUsername(){return _username;}
  getImagemPerfil(){return _imagemPerfil;}

  setId(int id){_id = id;}
  setNomeUsuario(String nomeUsuario){_nomeUsuario = nomeUsuario;}
  setEmail(String email){_email = email;}
  setSenha(String senha){_senha = senha;}
  setAltura(int altura){_altura = altura;}
  setPeso(double peso){_peso = peso;}
  setUsername(String username){_username = username;}
  setImagemPerfil(String imagem){_imagemPerfil = imagem;}

}
