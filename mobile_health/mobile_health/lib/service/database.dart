import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/models/atividade.dart';
import 'package:mobile_health/models/pesagem.dart';
import 'package:mobile_health/models/postagem.dart';
import 'package:mobile_health/models/refeicao.dart';
import 'package:mobile_health/models/usuario.dart';
import 'package:mobile_health/service/sharedpreference.dart';
import 'package:mysql1/mysql1.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';

class DatabaseService{


  static var conexao;

  DatabaseService(){
    init();
  }

  void init() async {
    if (conexao == null) {
      await criarConexao();
    }
  }

  static var settings = new ConnectionSettings(
      host: 'www.db4free.net',
      port: 3306,
      user: 'mobile_health',
      password: 'mobilehealth',
      db: 'mobile_health'
  );

  criarConexao() async {
    conexao = await MySqlConnection.connect(settings);
  }

  Future<int> cadastrarUsuario(Usuario u) async{

    var result;
    try {
      result = await conexao.query(
          "INSERT INTO usuarios (nome_usuario, email, senha, altura, peso, username)"
              "value (?,?,?,?,?,?)",
          [
            u.getNomeUsuario(),
            u.getEmail(),
            u.getSenha(),
            u.getAltura(),
            u.getPeso(),
            u.getUsername()
          ]);
    }catch(e){
      print(e.toString());
      return null;
    }
      return result.insertId;

  }

  usernameJaExiste(String username) async {

    var result = await conexao.query("select username from usuarios where username = '$username';");

    bool jaExiste = false;
    for (var row in result) {
      jaExiste = true;
      break;
    }

    return jaExiste;
  }

  emailJaExiste(String email) async {

    var result = await conexao.query("select * from usuarios where email = '$email';");

    bool jaExiste = false;
    for (var row in result) {
      jaExiste = true;
      break;
    }

    return jaExiste;
  }

  efetuarLogin(String email, String senha) async {
      var result = await conexao.query("SELECT * FROM usuarios WHERE email = '$email' and senha = '$senha';");

      Usuario u = null;
      for (var row in result) {
        u = Usuario();
        u.setId(row[0]);
        u.setNomeUsuario(row[1]);
        break;
      }

      return u;
  }

  Future<int> cadastrarPeso(Pesagem p) async {
    String id = await readStringSharedPreference(ID_USUARIO);
    p.setIdUsuario(int.parse(id));

    var result;
    try {
      result = await conexao.query(
          "INSERT INTO pesagem (id_usuario, peso, data)"
              "value (?,?,?)",
          [
            p.getIdUsuario(),
            p.getPeso(),
            p.getData()
          ]);
    } catch (e) {
      print(e.toString());
      return null;
    }
    return result.insertId;
  }

  Future<List<Pesagem>> listarPesagens() async {

    int id_usuario = await readStringSharedPreference(ID_USUARIO);
    List<Pesagem> pesos = List();

    var result;
    try {
      result = await conexao.query("select peso, data from pesagem where id_usuario = '$id_usuario';");
      for (var row in result) {
        Pesagem p = Pesagem();
        p.setPeso(row[0]);
        p.setData(row[1]);
        pesos.add(p);
      }

    } catch (e) {
      print(e.toString());
      return null;
    }
    return pesos;
  }

  Future<int> cadastrarAtividade(Atividade a) async {

    String id = await readStringSharedPreference(ID_USUARIO);
    a.setIdUsuario(int.parse(id));

    var result;
    try {
      result = await conexao.query(
          "INSERT INTO atividades (id_usuario, titulo_atividade, exercicio, duracao, distancia, caloria, data, dia_semana, semana )"
              "value (?,?,?,?,?,?,?,?,?)",
          [
            a.getIdUsuario(),
            a.getTituloAtividade(),
            a.getExercicio(),
            a.getDuracao(),
            a.getDistancia(),
            a.getCalorias(),
            a.getData(),
            a.getDiaSemana(),
            a.getSemanaAno()
          ]);
    } catch (e) {
      print(e.toString());
      return null;
    }
    return result.insertId;
  }

  Future<List<Atividade>> listarAtividadeFisica() async {
    int id_usuario = await readStringSharedPreference(ID_USUARIO);
    List<Atividade> atividades = List();
    var result;
    try {
      result = await conexao.query("select 	titulo_atividade, exercicio, duracao, distancia from atividades where id_usuario = '$id_usuario';");
      for (var row in result) {
        Atividade a = Atividade();
        a.setTituloAtividade(row[0]);
        a.setExercicio(row[1]);
        a.setDuracao(row[2]);
        a.setDistancia(row[3]);
        atividades.add(a);
      }

    } catch (e) {
      print(e.toString());
      return null;
    }
    return atividades;
  }

  Future<int> cadastrarRefeicoes(Refeicao r) async {

    String id = await readStringSharedPreference(ID_USUARIO);
    r.setIdUsuario(int.parse(id));

    var result;
    try {
      result = await conexao.query(
          "INSERT INTO refeicoes (id_usuario, tipo_refeicao, alimento, quantidade, medida, caloria, dia_semana, semana)"
              "value (?,?,?,?,?,?,?,?)",
          [
            r.getIdUsuario(),
            r.getTipoRefeicao(),
            r.getAlimento(),
            r.getQuantidade(),
            r.getMedida(),
            r.getCaloria(),
            r.getDiaSemana(),
            r.getSemanaAno()
          ]);
    } catch (e) {
      print(e.toString());
      return null;
    }
    return result.insertId;
  }

  Future<List<Refeicao>> consultarRefeicoes(String data) async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    List<Refeicao> refeicoes = List();
    var result;
    try {
      result = await conexao.query("SELECT tipo_refeicao, alimento, quantidade, medida, caloria FROM refeicoes WHERE id_usuario = ${id_usuario} and data = '${data}' ORDER by tipo_refeicao ASC;");
      for (var row in result) {
        Refeicao r = Refeicao();
        r.setTipoRefeicao(row[0]);
        r.setAlimento(row[1]);
        r.setQuantidade(row[2]);
        r.setMedida(row[3]);
        r.setCaloria(row[4]);
        refeicoes.add(r);
      }

    } catch (e) {
      print(e.toString());
      return null;
    }
    return refeicoes;
  }

  Future<List<Refeicao>> consultarCaloriaSemana(int semana) async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    List<Refeicao> caloriasDias = List();
    var result;
    try {
      result = await conexao.query("select dia_semana, sum(caloria) as 'caloria' from refeicoes where id_usuario = ${id} and semana = ${semana} GROUP BY dia_semana ORDER BY dia_semana ASC;");

      for (var row in result) {
        Refeicao r = Refeicao();
        r.setDiaSemana(row[0]);
        r.setCaloria(row[1]);
        caloriasDias.add(r);
      }

    } catch (e) {
      print(e.toString());
      return null;
    }

    return caloriasDias;
  }

  consultarCaloriasIngeridasNoDia(String data) async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    double caloria = null;
    var result;
    try {
      result = await conexao.query(
          "SELECT SUM(caloria) FROM refeicoes WHERE id_usuario = ${id_usuario} and data = '${data}';");

      for (var row in result) {
        caloria = row[0];
        break;
      }
    } catch (e) {
      print(e.toString());

      return null;
    }

    return caloria;
  }

  Future<bool> cadastrarListaRefeicoes(List<Refeicao> listaRefeicoes) async {

    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    String sql = "";

    for(var i = 0; i < listaRefeicoes.length; i++){
      sql = "INSERT INTO refeicoes (id_usuario, tipo_refeicao, alimento, quantidade, medida, caloria, data, dia_semana, semana)"
          "value (${id},"
          "'${listaRefeicoes[i].getTipoRefeicao()}',"
          "'${listaRefeicoes[i].getAlimento()}',"
          "${listaRefeicoes[i].getQuantidade()},"
          "'${listaRefeicoes[i].getMedida()}',"
          "${listaRefeicoes[i].getCaloria()},"
          "'${listaRefeicoes[i].getData()}',"
          "${listaRefeicoes[i].getDiaSemana()},"
          "${listaRefeicoes[i].getSemanaAno()});";

      var result;
      try {
        result = await conexao.query(sql);
      } catch (e) {
        print(e.toString());
        return false;
      }
    }

    return true;
  }

  Future<Usuario> buscarInformacaoUsuario() async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    var result = await conexao.query(
        "SELECT altura, peso FROM usuarios WHERE id = ${id_usuario};");

    Usuario u = Usuario();
    for (var row in result) {
      u.setAltura(row[0]);
      u.setPeso(row[1]);
      break;
    }

    return u;
  }

  buscarPesoMaisRecente() async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    var result = await conexao.query(
        "SELECT peso FROM pesagem p INNER JOIN (SELECT MAX(id) as ID_CONSULTA FROM pesagem p WHERE id_usuario = ${id_usuario}) as temp ON p.id = ID_CONSULTA;");

    double peso = null;
    for (var row in result) {
      peso = row[0];
      break;
    }

    return peso;
  }

  buscarExercicioMaisRecente() async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    var result = await conexao.query(
        "SELECT exercicio , caloria FROM atividades a INNER JOIN (SELECT MAX(id) as ID_CONSULTA FROM atividades WHERE id_usuario = ${id_usuario}) as temp ON a.id = ID_CONSULTA;");

    Atividade a = Atividade();
    for (var row in result) {
      a.setExercicio(row[0]);
      a.setCalorias(row[1]);
      break;
    }

    return a;
  }


  Future<List<Atividade>> queimaCaloricaAtividadeSemanal() async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    List<Atividade> calorias = List();

    var result = await conexao.query(
        "SELECT SUM(caloria), semana FROM atividades WHERE id_usuario = ${id_usuario} GROUP BY semana ORDER BY semana ASC;");

    for (var row in result) {
      Atividade a = Atividade();
      a.setCalorias(row[0]);
      a.setSemanaAno(row[1]);
      calorias.add(a);
    }

    return calorias;
  }

  Future<int> cadastrarPostagem(Postagem p) async {
    String id = await readStringSharedPreference(ID_USUARIO);
    p.setIdUsuario(int.parse(id));

    var result;
    try {

      String username;
      result = await conexao.query("SELECT username from usuarios where id = ${id};");

      for (var row in result) {
          username = row[0];
      }

      result = await conexao.query(
          "INSERT INTO postagens (id_usuario, texto_publicacao, imagem_publicada, username)"
              "value (?,?,?,?)",
          [
            p.getIdUsuario(),
            p.getTextoPublicacao(),
            p.getImagem(),
            username
          ]);
    } catch (e) {
      print(e.toString());
      return null;
    }
    return result.insertId;
  }

  Future<List<Postagem>> listarPostagens() async {
    String id = await readStringSharedPreference(ID_USUARIO);
    int id_usuario = int.parse(id);

    List<Postagem> postagens = List<Postagem>();
    try {
      var result = await conexao.query("SELECT username, texto_publicacao, imagem_publicada FROM postagens ORDER BY id DESC;");



      final filename = 'file.txt';

      for (var row in result) {
        Postagem p = Postagem();
        p.setUsername("${row[0]}");
        p.setTextoPublicacao("${row[1]}");
        String path = join(
          (await getTemporaryDirectory()).path,
          '${DateTime.now()}.png',
        );
        File f = File(path);
        f.writeAsBytesSync(base64Decode("${row[2]}"));
        p.setImagem(path);
        postagens.add(p);
      }
    } catch (e) {
      print(e.toString());
    }

    return postagens;
  }







}