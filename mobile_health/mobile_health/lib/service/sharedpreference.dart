import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/foundation.dart';


addStringToSharedPreference(String key, String value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString(key, value);
}

addBooleanToSharedPreference(String key, bool value) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setBool(key, value);
}

readStringSharedPreference(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  return prefs.getString(key) ?? "";
}

Future<bool> readBooleanSharedPreference(String key) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool a = await prefs.getBool(key) ?? false;
  return await prefs.getBool(key) ?? false;
}
