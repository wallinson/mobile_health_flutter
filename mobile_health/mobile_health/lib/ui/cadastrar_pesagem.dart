import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:intl/intl.dart';
import 'package:mobile_health/models/pesagem.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:toast/toast.dart';

class CadastrarPesagem extends StatefulWidget {

  @override
  _CadastrarPesagemState createState() => _CadastrarPesagemState();
}

class _CadastrarPesagemState extends State<CadastrarPesagem> {

  DateTime _date = DateTime.now();
  final _formater = new DateFormat('dd/MM/yyyy');

  bool loading = false;

  var pesoMask = new MoneyMaskedTextController(precision: 2, decimalSeparator: ".");
  TextEditingController _dataController = TextEditingController();

  _CadastrarPesagemState() {
    _dataController.text = _formater.format(_date);
  }

  Future<Null> selectDate(BuildContext context) async {
    debugPrint('Entrou');
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2018),
        lastDate: new DateTime.now(),
        locale: Locale("pt")
    );

    if (picked != null && picked != _date) {
      print("Data Selecionada: ${_date.toString()}");
      setState(() {
        _date = picked;
        _dataController.text = _formater.format(_date);
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    SingleChildScrollView body = SingleChildScrollView(
      child:
      Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: 300,
                width: 290,
                child: Align(
                  alignment: Alignment.center,
                  child: Image.asset(
                    "imgs/balanca3.png",
                    fit: BoxFit.cover,
                    height: 310,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(50, 25, 0, 0),
                child: Container(
                    height: 250,
                    width: 200,
                    child: Align(
                      alignment: Alignment.center,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(10, 60, 15, 0),
                        child: TextField(
                          controller: pesoMask,
                          autofocus: true,
                          keyboardType: TextInputType.number,
                          textAlign: TextAlign.center,
                          decoration: InputDecoration(
                            suffixText: "kg",
                            suffixStyle: TextStyle(
                                color: Colors.redAccent,
                                fontSize: 38
                            ),
                            labelStyle: TextStyle(
                              fontSize: 32,
                              color: Colors.redAccent,
                            ),
                            enabledBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.redAccent),
                            ),
                            focusedBorder: UnderlineInputBorder(
                              borderSide: BorderSide(color: Colors.redAccent),
                            ),
                          ),
                          style: TextStyle(
                              color: Colors.redAccent, fontSize: 38.0),
                          cursorColor: Colors.redAccent,
                          maxLength: 6,
                        ),
                      ),
                    )
                ),
              ),
              Column(
                children: <Widget>[
                ],
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(95, 0, 0, 0),
                child: Container(
                  width: 180,
                  child:
                  TextField(
                    decoration: InputDecoration(
                        labelText: "Data Pesagem:",
                        suffixIcon: IconButton(
                          icon: Icon(Icons.calendar_today),
                          color: Colors.redAccent,
                          onPressed: () async{
                            selectDate(context);
                          },
                        ),
                        labelStyle: TextStyle(
                            fontSize: 16
                        )
                    ),
                    controller: _dataController,
                    readOnly: true,
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: FlatButton(
              color: Colors.red,
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.all(8.0),
              splashColor: Colors.redAccent,
              onPressed: () async {
                if(pesoMask.text!="0.00"){
                  setState(() {
                    loading = true;
                  });

                  Pesagem p  = Pesagem();
                  p.setPeso(double.parse(pesoMask.text));
                  p.setData(_dataController.text);
                  int id = await DatabaseService().cadastrarPeso(p);

                  setState(() {
                    loading = false;
                  });

                  if(id!=null){
                    Toast.show(
                        "Peso cadastrado com sucesso!!!", context, duration: 3,
                        gravity: Toast.BOTTOM);
                  }else{
                    Toast.show(
                        "Não foi possível cadastrar o peso, tente mais tarde!!!",
                        context, duration: 3, gravity: Toast.BOTTOM);
                  }
                }else{
                  Toast.show(
                      "Insira um peso válido!!!", context, duration: 1,
                      gravity: Toast.CENTER);
                }
              },
              child: Text(
                "Cadastrar",
                style: TextStyle(fontSize: 18.0),
              ),
            ),
          ),
        ],
      ),
    );

    return loading ? Loading() : Scaffold(
        appBar: new AppBar(title: new Text('Novo Peso')), body: body);
  }
}
