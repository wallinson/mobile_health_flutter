import 'package:flutter/material.dart';
import 'package:grouped_buttons/grouped_buttons.dart';
import 'package:intl/intl.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/models/refeicao.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/autocomplete_textfield.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:toast/toast.dart';
import 'package:mobile_health/utils/date_utils.dart';

class CadastrarAlimentacao extends StatefulWidget {

  String refeicao;

  CadastrarAlimentacao(this.refeicao);

  @override
  _CadastrarAlimentacaoState createState() =>
      _CadastrarAlimentacaoState(refeicao);
}

class _CadastrarAlimentacaoState extends State<CadastrarAlimentacao> {
  String dropdownValue = 'Refeição';
  String dropdownValueQuantidade = 'Medida';
  String refeicao;
  List<String> added = [];
  String currentText = "";
  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();

  final _validator = GlobalKey<FormState>();
  bool loading = false;

  DateTime _date = DateTime.now();
  final _formater = new DateFormat('dd/MM/yyyy');

  TextEditingController _alimentoController = TextEditingController();
  TextEditingController _quantidadeController = TextEditingController();
  String unidadeMedida = "";
  List<Refeicao> listaRefeicoes = List();

  SimpleAutoCompleteTextField textField;

  _CadastrarAlimentacaoState(String refeicao) {
    this.refeicao = refeicao;
    textField = SimpleAutoCompleteTextField(
      key: key,
      style: TextStyle(fontSize: 16, height: 1.0),
      decoration: InputDecoration(labelText: "Alimento:"),
      controller: _alimentoController,
      suggestions: LISTA_ALIMENTO,
      textChanged: (text) => currentText = text,
      textSubmitted: (text) => setState(
            () {
          if (text != "") {
            added.add(text);
          }
        },
      ),
    );
  }

  clearFields() {
    setState(() {
      _alimentoController.text = "";
      _quantidadeController.text = "";
    });
  }

  @override
  Widget build(BuildContext context) {
    Column body = Column(
      children: <Widget>[
        ListTile(
          title: textField,
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 8, 0),
          child: Row(
            children: <Widget>[
              Container(
                width: 100,
                child: TextField(
                  controller: _quantidadeController,
                  decoration: InputDecoration(labelText: "Quantidade:"),
                  keyboardType: TextInputType.number,
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(80, 0, 0, 0),
                child: RadioButtonGroup(
                    orientation: GroupedButtonsOrientation.HORIZONTAL,
                    labels: <String>[
                      "g",
                      "ml",
                      "porção"
                    ],
                    onSelected: (String selected) => unidadeMedida = selected
                ),
              ),
            ],
          ),
        ),
        Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 15, 0),
              child: IconButton(
                icon: Icon(
                  Icons.add_circle, color: Colors.redAccent, size: 40,
                ), onPressed: () {
                if (_quantidadeController.text != "" &&
                    _alimentoController.text != "") {
                  setState(() {
                    Refeicao r = Refeicao();
                    r.setTipoRefeicao(refeicao);
                    r.setAlimento(_alimentoController.text);
                    r.setQuantidade(double.parse(_quantidadeController.text));
                    r.setMedida(unidadeMedida);
                    int a = (refeicao.length * 4) +
                        (_alimentoController.text.length * 5) + (int.parse(_quantidadeController.text)*5);
                    r.setCaloria(a + 1.0);
                    r.setData(_formater.format(_date));
                    r.setDiaSemana(DateUtils.numeroDiaSemana());
                    r.setSemanaAno(DateUtils.numeroSemanaAno());
                    listaRefeicoes.add(r);
                    clearFields();
                  });
                } else {
                  Toast.show(
                      "O campo Alimento e Quantidade devem ser preenchidos.",
                      context, duration: 2, gravity: Toast.CENTER);
                }

              },
              ),
            ),
          ],
        ),
        Divider(),
        Column(
          children: <Widget>[
            SizedBox(
              height: 210,
              child: ListView.builder(
                itemCount: listaRefeicoes.length,
                itemBuilder: (context, indice) {
                  final refeicao = listaRefeicoes[indice];
                  return Card(
                    child: ListTile(
                      leading: Image.asset(
                        "imgs/alimento.png"
                        , fit: BoxFit.cover,
                        height: 60,),
                      title: Text(refeicao.getAlimento()),
                      subtitle: Text(
                          'Quantidade: ${refeicao.getQuantidade()} ${refeicao
                              .getMedida()} \n'
                              'Caloria: ${refeicao.getCaloria()} cal'),
                      isThreeLine: true,
                    ),
                  );
                },
              ),
            ),
          ],
        ),
        Divider(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              color: Colors.red,
              textColor: Colors.white,
              disabledColor: Colors.grey,
              disabledTextColor: Colors.black,
              padding: EdgeInsets.all(8.0),
              splashColor: Colors.redAccent,

              onPressed: () async {
                if (!listaRefeicoes.isEmpty) {
                  setState(() {
                    loading = true;
                  });
                  bool cadastrou = await DatabaseService()
                      .cadastrarListaRefeicoes(listaRefeicoes);

                    if (cadastrou) {
                      setState(() {
                        loading = false;
                      });
                      Toast.show("Refeição cadastrada com sucesso!!!", context,
                          duration: 4, gravity: Toast.BOTTOM);
                      listaRefeicoes = new List();
                      Navigator.pop(context,true);
                    } else {
                      setState(() {
                        loading = false;
                      });
                      Toast.show(
                          "Não foi possível cadastrar a refeição, tente mais tarde!!!",
                          context, duration: 3, gravity: Toast.BOTTOM);
                    }
                } else {
                  Toast.show(
                      "Não foram cadastrados itens para refeição!", context,
                      duration: 2, gravity: Toast.BOTTOM);
                }
              },
              child: Text(
                "Finalizar Cadastro",
                style: TextStyle(fontSize: 18.0),
              ),
            ),
          ],
        ),
      ],
    );

    return loading ? Loading() : Scaffold(
        appBar: new AppBar(title: new Text('Cadastro Refeição')),
        body: SingleChildScrollView(child: body,));
  }
}
