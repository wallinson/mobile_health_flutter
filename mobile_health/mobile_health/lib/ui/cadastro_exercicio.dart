import 'package:flutter/material.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/models/atividade.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:toast/toast.dart';
import 'package:intl/intl.dart';
import 'package:mobile_health/utils/date_utils.dart';

class CadastroExercicio extends StatefulWidget {
  @override
  _CadastroExercicioState createState() => _CadastroExercicioState();
}

class _CadastroExercicioState extends State<CadastroExercicio> {
  String dropdownValue = 'Exercício';

  List<String> added = [];
  String currentText = "";

  final key = GlobalKey<FormState>();
  bool loading = false;

  TextEditingController _tituloAtividade = TextEditingController();
  TextEditingController _duracao = TextEditingController();
  TextEditingController _distancia = TextEditingController();

  DateTime _date = DateTime.now();
  final _formater = new DateFormat('dd/MM/yyyy');

  clearFields(){
   setState(() {
     _tituloAtividade.text = "";
     dropdownValue = 'Exercício';
     _duracao.text="";
     _distancia.text="";
   });
  }

  @override
  Widget build(BuildContext context) {
    SingleChildScrollView body = SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Column(
            children: <Widget>[
              Form(
                key: key,
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                          child: Image.asset(
                            "imgs/runner.png",
                            fit: BoxFit.cover,
                            height: 180,
                          ),
                        ),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 180,
                            child: Column(
                              children: <Widget>[
                                Container(
                                  width: 180,
                                  child: TextFormField(
                                    controller: _tituloAtividade,
                                    validator: (val) =>
                                    val.isEmpty
                                        ? "Campo Titulo Atividade é obrigatório."
                                        : null,
                                    decoration: InputDecoration(
                                      hintText: "Título da Atividade"
                                    ),
                                    style: TextStyle(fontSize: 18),
                                  ),
                                ),
                                DropdownButton<String>(
                                  value: dropdownValue,
                                  icon: Icon(Icons.arrow_drop_down),
                                  iconSize: 24,
                                  hint: Text("Exercício:"),
                                  elevation: 20,
                                  underline: Container(
                                    height: 2,
                                    color: Colors.redAccent,
                                  ),
                                  onChanged: (String newValue) {
                                    setState(() {
                                      dropdownValue = newValue;
                                    });
                                  },
                                  items: LISTA_EXERCICIO
                                      .map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value,
                                          style: TextStyle(
                                              fontSize: 20.0
                                          ),
                                        ),
                                      );
                                    },
                                  ).toList(),
                                ),
                                TextFormField(
                                  controller: _duracao,
                                  validator: (val) =>
                                  val.isEmpty
                                      ? "Campo Duracao é obrigatório."
                                      : null,
                                  decoration: InputDecoration(
                                      hintText: "Duração em minutos",),
                                  style: TextStyle(fontSize: 18),
                                  keyboardType: TextInputType.number,
                                ),
                                TextField(
                                  controller: _distancia,
                                  decoration: InputDecoration(
                                    hintText: "Distância (opcional)",
                                    suffixText: "m",
                                  ),
                                  style: TextStyle(fontSize: 18,),
                                  keyboardType: TextInputType.number,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              FlatButton(
                color: Colors.red,
                textColor: Colors.white,
                disabledColor: Colors.grey,
                disabledTextColor: Colors.black,
                padding: EdgeInsets.fromLTRB(0, 8, 0, 0),
                splashColor: Colors.redAccent,
                onPressed: () async {
                  if (key.currentState.validate()) {
                    setState(() {
                      loading = true;
                    });

                    Atividade a = Atividade();
                    a.setTituloAtividade(_tituloAtividade.text);
                    a.setExercicio(dropdownValue);
                    a.setDuracao(double.parse(_duracao.text));
                    a.setData(_formater.format(_date));
                    double calc = 72.0 + (int.parse(_duracao.text)*10);
                    a.setCalorias(calc);
                    a.setDiaSemana(DateUtils.numeroDiaSemana());
                    a.setSemanaAno(DateUtils.numeroSemanaAno());


                    String distancia = _distancia.text;
                    if (distancia != null && distancia != "") {
                      a.setDistancia(double.parse(distancia));
                    }

                    int id = await DatabaseService().cadastrarAtividade(a);

                    setState(() {
                      loading = false;
                    });

                    if (id != null) {
                      Toast.show("Peso cadastrado com sucesso!!!", context,duration: 4, gravity: Toast.BOTTOM);
                      clearFields();
                    } else {
                      Toast.show(
                          "Não foi possível cadastrar refeicao, tente mais tarde!!!",
                          context, duration: 4, gravity: Toast.BOTTOM);
                    }
                  }
                 
                },
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0, 0, 0, 8),
                  child: Text(
                    "Cadastrar",
                    style: TextStyle(fontSize: 18.0),
                  ),
                ),
              ),
              Divider(),
            ],
          )
        ],
      ),
    );

    return loading ? Loading() : Scaffold(
        appBar: new AppBar(title: new Text('Cadastro Exercício')), body: body);
  }
}
