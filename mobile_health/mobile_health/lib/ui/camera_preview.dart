/*
import 'dart:async';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart' show join;
import 'package:path_provider/path_provider.dart';
import 'package:image_picker_saver/image_picker_saver.dart';
import 'package:gallery_saver/gallery_saver.dart';

List<CameraDescription> cameras;

class TirarFoto extends StatefulWidget {
  TirarFotoState createState() => TirarFotoState();
}

class TirarFotoState extends State<TirarFoto> {

  void _takePhoto(String path) async {
    ImagePickerSaver.pickImage(source: ImageSource.camera)
        .then((File recordedImage) {
      if (recordedImage != null && recordedImage.path != null) {
        GallerySaver.saveImage(recordedImage.path);
      }
    });
  }

  Widget build(BuildContext context) {
    return Scaffold(
        body: CameraPreview(controller),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.camera_alt),

          onPressed: () async {
            try {
              final path = join(
                (await getApplicationDocumentsDirectory()).path,
                '${DateTime.now()}.png',
              );

              await controller.takePicture(path);
              var imagen = await ImagePickerSaver.pickImage(source: ImageSource.camera);
              File savedImage = await imagen.copy(path);

              Navigator.pop(context, path);

            } catch (e) {
              print(e);
            }
          },
        ),
    );
  }
}*/

