import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobile_health/models/postagem.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:mobile_health/ui/nova_postagem.dart';
import 'package:toast/toast.dart';

class Comunidade extends StatefulWidget {
  @override
  _ComunidadeState createState() => _ComunidadeState();
}

class _ComunidadeState extends State<Comunidade> {
  bool loading = false;
  Future<List<Postagem>> publicacoes() async {
    return await DatabaseService().listarPostagens();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Comunidade Health"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.share),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => NovaPostagem()));
            },
          )
        ],
      ),
      body: FutureBuilder<List<Postagem>>(
        future: publicacoes(),
        builder: (context, AsyncSnapshot<List<Postagem>> snapshot) {
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Loading();
            default:
              if (snapshot.data == null || snapshot.data.isEmpty) {
                return Center(
                  child: Text("Não foram cadastradas publicações"),
                );
              } else {
                return SizedBox(
                  height: 590,
                  child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, indice) {
                        final refeicao = snapshot.data[indice];
                        return Card(
                          child: Container(
                            width: 400,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(8, 8, 0, 5),
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset(
                                        "imgs/user.png",
                                        fit: BoxFit.fill,
                                        height: 20,),
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                                        child: Text(snapshot.data[indice].getUsername(),),
                                      )
                                    ],
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    /*Image.asset(
                                        "imgs/galeria.png",
                                    fit: BoxFit.fill,
                                    height: 150,),*/
                                    imagemPublicacao(snapshot.data[indice].getImagem()),

                                  ],
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(8, 10, 8, 10),
                                  child: Row(
                                    children: <Widget>[
                                      Text(
                                          "${snapshot.data[indice].getTextoPublicacao()}"),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                );
              }
              break;
          }
        },
      ),
    );
  }

  Widget imagemPublicacao(String path) {
    return path != null ? Image.file(
      File(path),
      fit: BoxFit.fill, height: 150,) : null;
  }
}
