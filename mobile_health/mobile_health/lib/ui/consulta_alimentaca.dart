import 'dart:async';
import 'dart:ui' as prefix0;

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobile_health/models/refeicao.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:mobile_health/ui/resultado_consulta_alimentacao.dart';
import 'package:mobile_health/ui/selecionar_refeicao.dart';
import 'package:mobile_health/utils/date_utils.dart';

class GraficoCalorias extends StatefulWidget {
  @override
  _GraficoCaloriasState createState() => _GraficoCaloriasState();
}

class _GraficoCaloriasState extends State<GraficoCalorias> {
  List<Refeicao> listaCaloriasDia;
  List<Refeicao> refeicoes;
  final diasSemana = [
    'Dom',
    'Seg',
    'Ter',
    'Qua',
    'Qui',
    'Sex',
    'Sab',
  ];
  List<FlSpot> listaDados;
  final double consumoCaloricoDiario = 2.2;
  bool loading = false;

  TextEditingController _dataController = TextEditingController();

  DateTime _date = DateTime.now();
  final _formater = new DateFormat('dd/MM/yyyy');

  List<FlSpot> carregarDadosGrafico() {
    List<FlSpot> dados = List();
    for (var i = 0; i < listaCaloriasDia.length; i++) {
      dados.add(FlSpot(i.toDouble(), listaCaloriasDia[i].getCaloria()));
    }
    return dados;
  }

  Future<List<FlSpot>> consultarCalorias() async {
    /*setState(() {
      loading = true;
    });*/

    listaCaloriasDia = await DatabaseService()
        .consultarCaloriaSemana(DateUtils.numeroSemanaAno());
    listaDados = await carregarDadosGrafico();
    listaDados = carregarDadosGrafico();

    /*setState(() {
      loading = false;
      listaDados = carregarDadosGrafico();listaDados = carregarDadosGrafico();
    });*/
    return listaDados;
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            appBar: AppBar(
              title: Text("Relatório de Calorias"),
            ),
            body: FutureBuilder<List<FlSpot>>(
                future: consultarCalorias(),
                builder: (context, AsyncSnapshot<List<FlSpot>> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Loading();
                    default:
                      return Scaffold(
                        body: snapshot.data == null || snapshot.data.isEmpty ?
                        Center(child: Text("Sem refeições cadastradas para essa semana!!!"),) :
                        Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(20, 30, 0, 0),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  Row(
                                    mainAxisSize: MainAxisSize.min,
                                    children: const <Widget>[
                                      Text(
                                        'Consumo Calórico Semanal',
                                        style: TextStyle(
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold,
                                            fontSize: 20),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 40,
                                  ),
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width - 40,
                                    height: 180,
                                    child: LineChart(
                                      LineChartData(
                                        lineTouchData: LineTouchData(
                                            getTouchedSpotIndicator:
                                                (LineChartBarData barData,
                                                    List<int> spotIndexes) {
                                              return spotIndexes
                                                  .map((spotIndex) {
                                                final FlSpot spot =
                                                    barData.spots[spotIndex];
                                                return TouchedSpotIndicatorData(
                                                  const FlLine(
                                                      color: Colors.blue,
                                                      strokeWidth: 3),
                                                  const FlDotData(
                                                      dotSize: 8,
                                                      dotColor:
                                                          Colors.deepOrange),
                                                );
                                              }).toList();
                                            },
                                            touchTooltipData:
                                                LineTouchTooltipData(
                                                    tooltipBgColor:
                                                        Colors.redAccent,
                                                    getTooltipItems:
                                                        (List<LineBarSpot>
                                                            touchedBarSpots) {
                                                      return touchedBarSpots
                                                          .map((barSpot) {
                                                        final flSpot = barSpot;

                                                        return LineTooltipItem(
                                                          '${diasSemana[flSpot.x.toInt()]} \n${flSpot.y} k colories',
                                                          const TextStyle(
                                                              color:
                                                                  Colors.white),
                                                        );
                                                      }).toList();
                                                    })),
                                        extraLinesData: ExtraLinesData(
                                            showVerticalLines: true,
                                            verticalLines: [
                                              VerticalLine(
                                                y: consumoCaloricoDiario,
                                                color: Colors.black12
                                                    .withOpacity(0.8),
                                                strokeWidth: 1.5,
                                              ),
                                            ]),
                                        lineBarsData: [
                                          LineChartBarData(
                                            spots: snapshot.data,
                                            isCurved: true,
                                            barWidth: 3,
                                            colors: [
                                              Colors.redAccent[200],
                                            ],
                                            belowBarData: BarAreaData(
                                                show: true,
                                                colors: [
                                                  Colors.redAccent
                                                      .withOpacity(0.5),
                                                  Colors.red.withOpacity(0.0),
                                                ],
                                                gradientColorStops: [0.5, 1.0],
                                                gradientFrom:
                                                    const Offset(0, 0),
                                                gradientTo: const Offset(0, 1),
                                                spotsLine: BarAreaSpotsLine(
                                                    show: true,
                                                    flLineStyle: const FlLine(
                                                      color: Colors.deepOrange,
                                                      strokeWidth: 2,
                                                    ),
                                                    checkToShowSpotLine:
                                                        (spot) {
                                                      return true;
                                                    })),
                                            dotData: FlDotData(
                                              show: true,
                                              dotColor: Colors.deepOrange,
                                              dotSize: 5,
                                            ),
                                          ),
                                        ],
                                        minY: 0,
                                        gridData: FlGridData(
                                          show: true,
                                          drawHorizontalGrid: true,
                                          drawVerticalGrid: true,
                                          getDrawingHorizontalGridLine:
                                              (value) {
                                            if (value == 0) {
                                              return const FlLine(
                                                color: Colors.deepOrange,
                                                strokeWidth: 2,
                                              );
                                            } else {
                                              return const FlLine(
                                                color: Colors.white,
                                                strokeWidth: 0.5,
                                              );
                                            }
                                          },
                                          getDrawingVerticalGridLine: (value) {
                                            if (value == 0) {
                                              return const FlLine(
                                                color: Colors.black,
                                                strokeWidth: 2,
                                              );
                                            } else {
                                              return const FlLine(
                                                color: Colors.grey,
                                                strokeWidth: 0.5,
                                              );
                                            }
                                          },
                                        ),
                                        titlesData: FlTitlesData(
                                            show: true,
                                            leftTitles: SideTitles(
                                              showTitles: false,
                                              getTitles: (value) {
                                                switch (value.toInt()) {
                                                  case 0:
                                                    return '';
                                                  case 1:
                                                    return '1Kcal';
                                                  case 2:
                                                    return '2Kcal';
                                                  case 3:
                                                    return '3Kcal';
                                                  case 4:
                                                    return '4Kcal';
                                                  case 5:
                                                    return '5Kcal';
                                                }

                                                return '';
                                              },
                                              textStyle: const TextStyle(
                                                  color: Colors.black,
                                                  fontSize: 10),
                                            ),
                                            bottomTitles: SideTitles(
                                              showTitles: true,
                                              getTitles: (value) {
                                                return diasSemana[
                                                    value.toInt()];
                                              },
                                              textStyle: const TextStyle(
                                                color: Colors.black,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            )),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: 280,
                              child: Row(
                                children: <Widget>[
                                  Expanded(
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(
                                          10.0, 20.0, 0.0, 0.0),
                                      child: TextField(
                                        controller: _dataController,
                                        decoration: InputDecoration(
                                          labelText: "Consultar Refeições",
                                          suffixIcon: IconButton(
                                            icon: Icon(Icons.calendar_today),
                                            color: Colors.redAccent,
                                            onPressed: () async {
                                              await selectDate(context);
                                              await consultarRefeicao(_date);
                                              debugPrint("Entrou");
                                              debugPrint(
                                                  "Entrou ${refeicoes.length}");
                                              setState(() {
                                                loading = false;
                                              });
                                            },
                                          ),
                                        ),
                                        style: TextStyle(fontSize: 16),
                                        readOnly: true,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(10, 20, 0, 0),
                                    child: ButtonTheme(
                                      minWidth: 20.0,
                                      height: 50.0,
                                      child: RaisedButton(
                                        onPressed: () {
                                          Navigator.push(context, MaterialPageRoute(builder: (context) => ResultadoConsultaAlimentacao( refeicoes, _formater.format(_date))));
                                        },
                                          color: Colors.redAccent,
                                        child: Icon(Icons.search, color: Colors.white,)
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                        floatingActionButton: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 20),
                          child: FloatingActionButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_context) =>
                                          SelecionarRefeicao()));
                            },
                            tooltip: 'Adicionar Refeicao',
                            child: Icon(Icons.fastfood),
                          ),
                        ),
                      );
                  }
                }),
          );
  }

  consultarRefeicao(DateTime data) async {
    refeicoes =
        await DatabaseService().consultarRefeicoes(_formater.format(data));
    return refeicoes;
  }

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2018),
        lastDate: new DateTime.now(),
        locale: Locale("pt"));

    if (picked != null) {
      setState(() {
        loading = true;
        _date = picked;
        _dataController.text = _formater.format(_date);
      });
    }
  }
}
