import 'package:async/async.dart';
import 'indicator.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobile_health/models/atividade.dart';
import 'package:mobile_health/models/refeicao.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/cadastro_exercicio.dart';
import 'package:mobile_health/ui/loading.dart';

class GraficoGastoCalorico extends StatefulWidget {
  @override
  _GraficoGastoCaloricoState createState() => _GraficoGastoCaloricoState();
}

class _GraficoGastoCaloricoState extends State<GraficoGastoCalorico> {
  List<Atividade> listaCaloriasMes;
  List<Refeicao> refeicoes;
  double totalCaloria;
  final double consumoCaloricoDiario = 2.2;

  bool loading = false;
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  TextEditingController _dataController = TextEditingController();

  DateTime _date = DateTime.now();
  final _formater = new DateFormat('dd/MM/yyyy');

  double calcularTotalCaloria(List<Atividade> listaCaloriasMes) {
    double total = 0.0;
    for (var i = 0; i < listaCaloriasMes.length; i++) {
      total += listaCaloriasMes[i].getCalorias();
    }
    return total;
  }

  Future<List<Atividade>> consultarCalorias() async {
    /*setState(() {
      loading = true;
    });*/

    listaCaloriasMes = await DatabaseService().queimaCaloricaAtividadeSemanal();
    totalCaloria = await calcularTotalCaloria(listaCaloriasMes);

    /*setState(() {
      loading = false;
      listaDados = carregarDadosGrafico();listaDados = carregarDadosGrafico();
    });*/
    return listaCaloriasMes;
  }

  int touchedIndex;

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            appBar: AppBar(
              title: Text("Relatório de Calorias"),
            ),
            body: FutureBuilder<List<Atividade>>(
                future: consultarCalorias(),
                builder: (context, AsyncSnapshot<List<Atividade>> snapshot) {
                  switch (snapshot.connectionState) {
                    case ConnectionState.waiting:
                      return Loading();
                    default:
                      return  Scaffold(
                        body: snapshot.data == null || snapshot.data.isEmpty ?
                        Center(child: Text("Sem exercícios cadastrados!!!"),) :
                        Column(
                          children: <Widget>[
                            Card(
                              color: Colors.white,
                              child: Column(
                                children: <Widget>[
                                  AspectRatio(
                                    aspectRatio: 0.7,
                                    child: Padding(
                                      padding: const EdgeInsets.fromLTRB(0, 30, 0, 0),
                                      child: Column(
                                        children: <Widget>[
                                          Text("Queima Calórica Semanal",style: TextStyle(fontSize: 22),),
                                          PieChart(
                                            PieChartData(
                                                pieTouchData: PieTouchData(
                                                    touchCallback:
                                                        (pieTouchResponse) {
                                                  setState(() {
                                                    if (pieTouchResponse.touchInput
                                                            is FlLongPressEnd ||
                                                        pieTouchResponse.touchInput
                                                            is FlPanEnd) {
                                                      touchedIndex = -1;
                                                    } else {
                                                      touchedIndex = pieTouchResponse
                                                          .touchedSectionIndex;
                                                    }
                                                  });
                                                }),
                                                borderData: FlBorderData(
                                                  show: false,
                                                ),
                                                sectionsSpace: 1,
                                                centerSpaceRadius: 40,
                                                sections: showingSections()),
                                          ),
                                          Column(
                                            mainAxisSize: MainAxisSize.max,
                                            mainAxisAlignment: MainAxisAlignment.end,
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: <Widget>[
                                              labeIndicator(0),
                                              labeIndicator(1),
                                              labeIndicator(2),
                                              labeIndicator(3),
                                            ],
                                          ),
                                          Divider(),
                                          const SizedBox(
                                            width: 28,
                                            child:
                                            Divider(),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        floatingActionButton: Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 20),
                          child: FloatingActionButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (_context) =>
                                          CadastroExercicio()));
                            },
                            tooltip: 'Adicionar Exercicio',
                            child: Icon(Icons.directions_run),
                          ),
                        ),
                      );
                  }
                }),
          );
  }

  List<Indicator> labelSemana(){
  List<Indicator> listaLabels = List();
  List<Color> listaCores = [Color(0xff0293ee), Color(0xfff8b250), Color(0xff845bef), Color(0xff13d38e)];

  for(int i = 0 ; i < listaCaloriasMes.length ; i++){
    listaLabels.add(
        Indicator(
          color: listaCores[i],
          text: '${i+1}º Semana',
          isSquare: true,
        )
    );
  }
      return listaLabels;
  }

  Widget labeIndicator(int indice){
    List<Color> listaCores = [Color(0xff0293ee), Color(0xfff8b250), Color(0xff845bef), Color(0xff13d38e)];
    return (listaCaloriasMes.length > indice) ? Indicator(
      color: listaCores[indice],
      text: '${indice+1}º Semana',
      isSquare: true,
    ) : Indicator(isSquare: true,text: "",);
  }


  consultarRefeicao(DateTime data) async {
    refeicoes =
        await DatabaseService().consultarRefeicoes(_formater.format(data));
    return refeicoes;
  }

  Future<Null> selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _date,
        firstDate: new DateTime(2018),
        lastDate: new DateTime.now(),
        locale: Locale("pt"));

    if (picked != null) {
      setState(() {
        loading = true;
        _date = picked;
        _dataController.text = _formater.format(_date);
      });
    }
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(listaCaloriasMes.length, (i) {
      final isTouched = i == touchedIndex;
      final double fontSize = isTouched ? 25 : 16;
      final double radius = isTouched ? 60 : 50;

      switch (i) {
        case 0:
          return i > listaCaloriasMes.length
              ? null
              : PieChartSectionData(
                  color: const Color(0xff0293ee),
                  value: listaCaloriasMes[i].getCalorias() / totalCaloria,
                  title: '${listaCaloriasMes[i].getCalorias()} cal',
                  radius: radius,
                  titleStyle: TextStyle(
                      fontSize: fontSize,
                      fontWeight: FontWeight.bold,
                      color: const Color(0xffffffff)),
                );
        case 1:
          return i > listaCaloriasMes.length
              ? null
              : PieChartSectionData(
                  color: const Color(0xfff8b250),
                  value: listaCaloriasMes[i].getCalorias() / totalCaloria,
                  title: '${listaCaloriasMes[i].getCalorias()} cal',
                  radius: radius,
                  titleStyle: TextStyle(
                      fontSize: fontSize,
                      fontWeight: FontWeight.bold,
                      color: const Color(0xffffffff)),
                );
        case 2:
          return i > listaCaloriasMes.length
              ? null
              : PieChartSectionData(
                  color: const Color(0xff845bef),
                  value: listaCaloriasMes[i].getCalorias() / totalCaloria,
                  title: '${listaCaloriasMes[i].getCalorias()} cal',
                  radius: radius,
                  titleStyle: TextStyle(
                      fontSize: fontSize,
                      fontWeight: FontWeight.bold,
                      color: const Color(0xffffffff)),
                );
        case 3:
          return i > listaCaloriasMes.length
              ? null
              : PieChartSectionData(
                  color: const Color(0xff13d38e),
                  value: listaCaloriasMes[i].getCalorias() / totalCaloria,
                  title: '${listaCaloriasMes[i].getCalorias()} cal',
                  radius: radius,
                  titleStyle: TextStyle(
                      fontSize: fontSize,
                      fontWeight: FontWeight.bold,
                      color: const Color(0xffffffff)),
                );
        case 4:
          return i > listaCaloriasMes.length
              ? null
              : PieChartSectionData(
                  color: Colors.redAccent,
                  value: listaCaloriasMes[i].getCalorias() / totalCaloria,
                  title: '${listaCaloriasMes[i].getCalorias()} cal',
                  radius: radius,
                  titleStyle: TextStyle(
                      fontSize: fontSize,
                      fontWeight: FontWeight.bold,
                      color: const Color(0xffffffff)),
                );
        default:
          return null;
      }
    });
  }
}
