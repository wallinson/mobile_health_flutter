import 'package:flutter/material.dart';
import 'package:mobile_health/ui/login.dart';
import 'package:mobile_health/ui/registrese.dart';
import 'package:mobile_health/service/database.dart';



class Entrar extends StatefulWidget {
  @override
  _EntrarState createState() => _EntrarState();
}

class _EntrarState extends State<Entrar> {

  DatabaseService db = DatabaseService();


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(52.0, 100.0, 8.0, 0),
            child: Row(
              children: <Widget>[
                Image.asset(
                  "imgs/logo.jpg",
                  fit: BoxFit.cover,
                  height: 150.0,
                ),
              ],
            ),
          ),
          Center(
            child: Container(
              height: 280.0,
              width: 150.0,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 180.0, 8.0, 0),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          FlatButton(
                            color: Colors.red,
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
                            disabledTextColor: Colors.black,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.redAccent,
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => Login(db)));
                            },
                            child: Text(
                              "Login",
                              style: TextStyle(fontSize: 24.0),
                            ),
                          ),
                        ],
                      ),
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: InkWell(
                              child: Text(
                                "Registre-se",
                                style: TextStyle(
                                  color: Colors.red,
                                  fontSize: 16.0,
                                ),
                              ),
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => Registro(DatabaseService())));
                              },
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
