import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/models/atividade.dart';
import 'package:mobile_health/models/usuario.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/service/sharedpreference.dart';
import 'package:mobile_health/ui/entrar.dart';
import 'package:mobile_health/ui/toolbar.dart';

class Home extends StatefulWidget {

  CameraDescription camera;

  lerCamera() async{
    final cameras = await availableCameras();
    final firstCamera = cameras.first;
    camera = await firstCamera;
  }

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  Future<Usuario> usuario;
  File _image = null;

  final TextEditingController _nomeControler = TextEditingController();
  String nome_usuario = "";
  String imagemPerfil = "imgs/user.png";
  DatabaseService db;

  String pesoInicial = "";
  String altura = "";
  String imc = "";
  String pesoAtual = "";
  String ultimoExercicio = "";
  String acumuladoCalorias = "";

  DateTime _date = DateTime.now();
  final _formater = new DateFormat('dd/MM/yyyy');


  Future<String> nomeUsuario()async{
    nome_usuario = await readStringSharedPreference(NOME_USUARIO);
    return nome_usuario;
  }

  @override
  void initState() {
    super.initState();
    inicializarBanco();
    usuario = carregarInfoUsuario();
    debugPrint("Entrou init");
    debugPrint("Entrou ${usuario.toString()}");
  }

  @override
  void dispose() {
    super.dispose();
  }

  inicializarBanco()async{
    db = await DatabaseService();
  }


  Future<Usuario> carregarInfoUsuario() async {
    Usuario u = await DatabaseService().buscarInformacaoUsuario();
    debugPrint("Entrou");
    altura = u.getAltura().toString();
    double _altura = u.getAltura() / 100;
    pesoInicial = u.getPeso().toString();
    double _peso = u.getPeso();
    imc = (_peso / (_altura * _altura)).toStringAsPrecision(2);
    debugPrint("Altura: ${altura}");
    debugPrint("Peso Inicial: ${pesoInicial}");
    debugPrint("Imc: ${imc}");

    return u;
  }

  Future<String> buscarPesoMaisRecente() async {
    double peso = await db.buscarPesoMaisRecente();
    pesoAtual = peso.toStringAsPrecision(2);
    debugPrint("Peso Atual: ${pesoAtual}");
    return pesoAtual;
  }

  Future<String> buscarExercicioMaisRecente() async {
    Atividade a = await db.buscarPesoMaisRecente();
    ultimoExercicio = a.getCalorias().toString();
    debugPrint("Exercicio: ${ultimoExercicio}");
    return ultimoExercicio;
  }

  Future<String> buscarAcumuladoCaloriaDia() async {
    double calorias = await db.consultarCaloriasIngeridasNoDia(
        _formater.format(_date));
    acumuladoCalorias = calorias.toString();
    debugPrint("Caloria: ${acumuladoCalorias}");
    return acumuladoCalorias;
  }

  Future<bool> carregarInformacaoPainelPrincipal() async {
    debugPrint("carregarInfoUsuario");
    await carregarInfoUsuario();
    debugPrint("buscarPesoMaisRecente");
    await buscarPesoMaisRecente;
    debugPrint("buscarExercicioMaisRecente");
    await buscarExercicioMaisRecente();
    debugPrint("buscarAcumuladoCaloriaDia");
    await buscarAcumuladoCaloriaDia();

    debugPrint("Antes Set");
    /*setState(() {
      pesoInicial;
      altura;
      imc;
      pesoAtual;
      ultimoExercicio;
      acumuladoCalorias;
    });*/
    debugPrint("Depois Set");
    return true;
  }

  getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      _image = image;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: toolbar(context),
      body: Padding(
        padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0.0, 10),
              child:
              Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 15, 0.0, 0),
                    child: Container(
                      width: 330,
                      height: 20,
                      child: Align(
                        alignment: Alignment.topRight,
                        child: IconButton(
                          icon: Padding(
                            padding: const EdgeInsets.fromLTRB(20, 0, 0.0, 0),
                            child: Icon(Icons.exit_to_app, size:
                            30,),
                          ),
                          alignment: Alignment.bottomCenter,
                          onPressed: () async {
                            addBooleanToSharedPreference(LOGADO, false);
                            Navigator.push(context, MaterialPageRoute(
                                builder: (context) => Entrar()));
                          },
                        ),
                      ),
                    ),
                  ),
                  Column(
                    children: <Widget>[
                      FutureBuilder(
                        future: nomeUsuario(),
                        builder: (context, snapshot) {
                          switch (snapshot.connectionState) {
                          // ignore: missing_return
                            case ConnectionState.waiting:
                              Text("Carregando dados...");
                              break;
                            default:
                              return Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(
                                        0, 0, 10.0, 0),
                                    child: Row(
                                      children: <Widget>[
                                        Row(
                                          children: <Widget>[
                                            Container(
                                              width: 120,
                                              height: 120,
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                image: new DecorationImage(
                                                  fit: BoxFit.fill,
                                                  image: _image == null
                                                      ? AssetImage(imagemPerfil)
                                                      : Image.file(_image),
                                                ),
                                                // ignore: missing_return
                                              ),
                                            ),
                                            Container(
                                              width: 20,
                                              height: 110,
                                              child: Align(
                                                alignment: Alignment
                                                    .bottomCenter,
                                                child: IconButton(
                                                  icon: Icon(Icons.edit),
                                                  iconSize: 20,
                                                  tooltip: 'Editar imagem perfil',
                                                  alignment: Alignment
                                                      .bottomCenter,
                                                  onPressed: () {
                                                    ImagePicker.pickImage(
                                                        source: ImageSource
                                                            .camera).then((
                                                        file) {
                                                      if (file == null)
                                                        return;
                                                      else {
                                                        setState(() {
                                                          _image = file;
                                                        });
                                                      }
                                                    });
                                                  },
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              0, 0, 0, 10),
                                          child: Column(
                                            children: <Widget>[
                                              Image.asset(
                                                "imgs/logInterno.png",
                                                fit: BoxFit.fill,
                                                height: 110,
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            15, 0, 0, 10),
                                        child: Text(snapshot.data,
                                          style: TextStyle(
                                            fontSize: 20,
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ],
                              );
                          }
                        },
                      ),
                    ],
                  ),
                  /*Column(
                        children: <Widget>[
                          SizedBox(
                            height: 280,
                            child: ListView(
                              children: const <Widget>[
                              ],
                            ),
                          ),
                        ],
                      ),*/
                ],
              ),
            ),
            Column(
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Column(children: <Widget>[
                      Card(
                        child: Container(
                          width: 120,
                          height: 120,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets
                                    .fromLTRB(0, 10, 0, 0),
                                child: Image.asset(
                                  "imgs/peso.png",
                                  fit: BoxFit.cover,
                                  height: 60,
                                ),
                              ),
                              Text("Peso Inicial",
                                style: TextStyle(
                                    fontSize: 18),),
                              Text("80Kg"),
                            ],
                          ),
                        ),
                      ),
                    ]
                      ,),
                    Column(children: <Widget>[
                      Card(
                        child: Container(
                          width: 120,
                          height: 120,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets
                                    .fromLTRB(0, 10, 0, 0),
                                child: Image.asset(
                                  "imgs/altura.png",
                                  fit: BoxFit.cover,
                                  height: 60,
                                ),
                              ),
                              Text("Altura", style: TextStyle(
                                  fontSize: 18),),
                              Text("1.80cm"),
                            ],
                          ),
                        ),
                      ),
                    ]
                      ,),
                    Column(children: <Widget>[
                      Card(
                        child: Container(
                          width: 120,
                          height: 120,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets
                                    .fromLTRB(0, 10, 0, 0),
                                child: Image.asset(
                                  "imgs/fitametrica.png",
                                  fit: BoxFit.cover,
                                  height: 60,
                                ),
                              ),
                              Text("IMC", style: TextStyle(
                                  fontSize: 18),),
                              Text("21.60"),
                            ],
                          ),
                        ),
                      ),
                    ]
                      ,),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(5, 0, 0, 0),
                  child: Row(
                    children: <Widget>[
                      Column(children: <Widget>[
                        Card(
                          child: Container(
                            width: 120,
                            height: 120,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets
                                      .fromLTRB(0, 10, 0, 0),
                                  child: Image.asset(
                                    "imgs/peso2.png",
                                    fit: BoxFit.cover,
                                    height: 60,
                                  ),
                                ),
                                Text("Peso Atual",
                                  style: TextStyle(
                                      fontSize: 18),),
                                Text("70Kg"),
                              ],
                            ),
                          ),
                        ),
                      ]
                        ,),
                      Column(children: <Widget>[
                        Card(
                          child: Container(
                            width: 120,
                            height: 120,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets
                                      .fromLTRB(0, 10, 0, 0),
                                  child: Image.asset(
                                    "imgs/runner.png",
                                    fit: BoxFit.cover,
                                    height: 60,
                                  ),
                                ),
                                Text("Exercício", style: TextStyle(
                                    fontSize: 18),),
                                Text("278cal"),
                              ],
                            ),
                          ),
                        ),
                      ]
                        ,),
                      Column(children: <Widget>[
                        Card(
                          child: Container(
                            width: 120,
                            height: 120,
                            child: Column(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets
                                      .fromLTRB(0, 10, 0, 0),
                                  child: Image.asset(
                                    "imgs/alimento.png",
                                    fit: BoxFit.cover,
                                    height: 60,
                                  ),
                                ),
                                Text("Refeição", style: TextStyle(
                                    fontSize: 18),),
                                Text("1800cal"),
                              ],
                            ),
                          ),
                        ),
                      ]
                        ,),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}