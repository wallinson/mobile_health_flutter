import 'package:flutter/material.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/home.dart';
import 'package:mobile_health/service/sharedpreference.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/models/usuario.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:toast/toast.dart';


class Login extends StatefulWidget {

  DatabaseService db;
  Login(this.db);

  @override
  _LoginState createState() => _LoginState(db);
}

class _LoginState extends State<Login> {
  final TextEditingController _emailControler = TextEditingController();
  final TextEditingController _senhaControler = TextEditingController();
  final _validator = GlobalKey<FormState>();
  bool loading = false;

  String error = "";

  DatabaseService db;

  _LoginState(this.db);

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        child: Form(
          key: _validator,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(42.0, 80.0, 8.0, 0),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      "imgs/logo.jpg",
                      fit: BoxFit.cover,
                      height: 150.0,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: _emailControler,
                      validator: (val) =>
                      val.isEmpty
                          ? 'Campo Email é obrigatório.'
                          : null,
                      decoration: InputDecoration(
                        labelText: "Email: ",
                        hintText: "your@email.com",
                      ),
                    ),
                    TextFormField(
                      obscureText: true,
                      validator: (val) =>
                      val.isEmpty
                          ? 'Campo Senha é obrigatório.'
                          : null,
                      controller: _senhaControler,
                      decoration: InputDecoration(
                          labelText: "Senha: ",
                          hintText: "*******"
                      ),
                    ),
                    Container(
                      height: 75,
                      width: 300,
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0),
                          child: FlatButton(
                            color: Colors.red,
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
                            disabledTextColor: Colors.black,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.redAccent,
                            onPressed: () async {
                             if(_validator.currentState.validate()){

                               setState(() {
                                 loading = true;
                               });

                                  Usuario u = await db.efetuarLogin(_emailControler.text, _senhaControler.text);

                                  if(u!=null){
                                    List<String> aux = u.getNomeUsuario().split(" ");
                                    int tamanho = aux.length - 1;
                                    String nome;
                                    if(tamanho>0){
                                      nome = "${u.getNomeUsuario().split(" ")[0]} ${u.getNomeUsuario().split(" ")[tamanho]}";
                                    }else{
                                      nome = u.getNomeUsuario();
                                    }
                                    addStringToSharedPreference(ID_USUARIO, u.getId().toString());
                                    addStringToSharedPreference(NOME_USUARIO,nome);
                                    addBooleanToSharedPreference(LOGADO, true);

                                    Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                                  }else{
                                    setState(() {
                                      loading = false;
                                    });
                                    Toast.show("Email e/ou senha incorretos.", context, duration: 4, gravity:  Toast.BOTTOM);
                                  }
                             }
                            },
                            child: Text(
                              "Entrar",
                              style: TextStyle(fontSize: 18.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
