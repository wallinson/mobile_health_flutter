import 'dart:convert';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobile_health/ui/autocomplete_textfield.dart';
import 'package:mobile_health/ui/indicator.dart';
import 'package:mobile_health/models/postagem.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:toast/toast.dart';
import 'package:mobile_health/ui/comunidade.dart';
import 'package:mobile_health/ui/sucesso_postagem.dart';


class NovaPostagem extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Nova Postagem',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  File _image;
  TextEditingController _textoPostagemController = TextEditingController();
  bool loading = false;
  int _id;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera,imageQuality: 40);

    setState(() {
      _image = image;
    });
  }

  publicar() async {
    if(_textoPostagemController.text.isEmpty && _image==null){
      Toast.show("Não é possível criar publicação vazia.", context, duration: 2, gravity: Toast.TOP);
    }else {
      Postagem p = Postagem();
      p.setTextoPublicacao(_textoPostagemController.text);
      p.setImagem(base64Encode(_image.readAsBytesSync()));

      _id = await DatabaseService().cadastrarPostagem(p);
    }
    return _id;
  }



  @override
  Widget build(BuildContext context) {
    return loading ? Loading() : Scaffold(
      appBar: AppBar(
        title: Text('Nova Postagem'),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.send),
            onPressed: () async {
              setState(() {
                loading = true;
              });
              await publicar();

              if(_id!=null){
                Navigator.push(context, MaterialPageRoute(builder: (context) => SucessoPostagem())).then((t){
                  Navigator.pop(context);
                });
              }else{
                setState(() {
                  loading = false;
                });
                Toast.show("Não foi possível realiza a publicação.\nTente novamente mais tarde.", context, duration: 3, gravity: Toast.CENTER);
              }
            },
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Card(
              child: Container(
                width: 400,
                height: 150,
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    controller: _textoPostagemController,
                    maxLength: 140,
                    maxLines: 5,
                    keyboardType: TextInputType.multiline,
                    decoration: InputDecoration(labelText: "Texto Publicação"),
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Center(
                child: Container(
                  width: 400,
                  height: 300,
                  child: _image == null
                      ? Text('Sem imagem selecionada.')
                      : Image.file(_image),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){
          ImagePicker.pickImage(source: ImageSource.camera).then((file){
            if(file == null) return;
            else {
              setState(() {
                _image = file;
              });
            }
          });
        },
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
      ),
    );
  }
}
