import 'package:flutter/material.dart';
import 'package:mobile_health/models/usuario.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/ui/tela_cadastro_usuario.dart';
import 'package:toast/toast.dart';
import 'package:mobile_health/ui/loading.dart';


class Registro extends StatefulWidget {

  DatabaseService db;
  Registro(this.db);

  @override
  _RegistroState createState() => _RegistroState(db);
}

class _RegistroState extends State<Registro> {
  final TextEditingController _emailControler = TextEditingController();
  final TextEditingController _senhaControler = TextEditingController();
  final _validator = GlobalKey<FormState>();
  bool loading = false;

  String error = "";

  DatabaseService db;
  _RegistroState(this.db);

  @override
  Widget build(BuildContext context) {
    return loading ? Loading() :  Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        child: Form(
          key: _validator,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(40.0, 80.0, 8.0, 0),
                child: Row(
                  children: <Widget>[
                    Image.asset(
                      "imgs/logo.jpg",
                      fit: BoxFit.cover,
                      height: 150.0,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: _emailControler,
                      validator: (val) => val.isEmpty ? 'Campo email é obrigatório': null,
                      decoration: InputDecoration(
                        labelText: "Email: ",
                        hintText: "your@email.com",
                      ),
                    ),
                    TextFormField(
                      obscureText: true,
                      controller: _senhaControler,
                      validator: (val) => val.isEmpty ? 'Campo senha é obrigatório': null,
                      decoration: InputDecoration(
                          labelText: "Senha: ", hintText: "*******"),
                    ),
                    Container(
                      height: 75,
                      width: 300,
                      child: Align(
                        alignment: Alignment.topRight,
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0),
                          child: FlatButton(
                            color: Colors.red,
                            textColor: Colors.white,
                            disabledColor: Colors.grey,
                            disabledTextColor: Colors.black,
                            padding: EdgeInsets.all(8.0),
                            splashColor: Colors.redAccent,
                            onPressed: () async {

                              if (_validator.currentState.validate()) {

                                setState(() {
                                  loading = true;
                                });

                                Usuario u = Usuario();
                                u.setEmail(_emailControler.text);
                                u.setSenha(_senhaControler.text);

                                bool existe = await db.emailJaExiste(_emailControler.text);
                                if (!existe) {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              CadastroUsuario(db,u)));
                                } else {
                                  setState(() {
                                    loading = false;
                                  });
                                  Toast.show("O email informado ja está em uso.", context, duration: 4, gravity:  Toast.BOTTOM);
                                }
                              }
                            },
                            child: Text(
                              "Cadastrar",
                              style: TextStyle(fontSize: 18.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }


}
