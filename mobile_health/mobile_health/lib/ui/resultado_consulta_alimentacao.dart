import 'package:flutter/material.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/models/refeicao.dart';

class ResultadoConsultaAlimentacao extends StatefulWidget {
  String data;
  List<Refeicao> refeicoes;

  ResultadoConsultaAlimentacao(this.refeicoes, this.data);

  @override
  _ResultadoConsultaAlimentacaoState createState() =>
      _ResultadoConsultaAlimentacaoState(refeicoes, data);
}

class _ResultadoConsultaAlimentacaoState
    extends State<ResultadoConsultaAlimentacao> {
  String data;
  List<Refeicao> refeicoes;

  _ResultadoConsultaAlimentacaoState(this.refeicoes, this.data) {
    itensCafe = separarItensRefeicao(REFEICAO_CAFE);
    itensAlmoco = separarItensRefeicao(REFEICAO_ALMOCO);
    itensLanche = separarItensRefeicao(REFEICAO_LANCHE);
    itensJanta = separarItensRefeicao(REFEICAO_JANTA);
  }

  String itensCafe = "";
  String itensAlmoco = "";
  String itensLanche = "";
  String itensJanta = "";

  separarItensRefeicao(String tipo_refeicao) {
    String s = "";
    for (var row in refeicoes) {
      s = s + "- Alimento: ${row.getAlimento()}\n";
      s = s + "\t \t - Caloria: ${row.getCaloria()}\n";
      s = s + "\t \t - Quantidade: ${row.getQuantidade()} ${row.getMedida()}\n";
    }
    return s;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Refeições ${data}"),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  REFEICAO_CAFE,
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  !itensCafe.isEmpty
                      ? itensCafe
                      : "Sem itens cadastrados para esta refeição.",
                  style: TextStyle(fontSize: 14, color: Colors.black45),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text(
                  REFEICAO_ALMOCO,
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  !itensAlmoco.isEmpty
                      ? itensAlmoco
                      : "Sem itens cadastrados para esta refeição.",
                  style: TextStyle(fontSize: 14, color: Colors.black45),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text(
                  REFEICAO_JANTA,
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  !itensJanta.isEmpty
                      ? itensJanta
                      : "Sem itens cadastrados para esta refeição.",
                  style: TextStyle(fontSize: 14, color: Colors.black45),
                ),
              ],
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text(
                  REFEICAO_LANCHE,
                  style: TextStyle(fontSize: 22, color: Colors.black),
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Text(
                  !itensLanche.isEmpty
                      ? itensLanche
                      : "Sem itens cadastrados para esta refeição.",
                  style: TextStyle(fontSize: 14, color: Colors.black45),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
