import 'package:flutter/material.dart';
import 'package:mobile_health/ui/cadastro_alimentacao.dart';
import 'package:mobile_health/constantes/constantes.dart';

class SelecionarRefeicao extends StatefulWidget {
  @override
  _SelecionarRefeicaoState createState() => _SelecionarRefeicaoState();
}

class _SelecionarRefeicaoState extends State<SelecionarRefeicao> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(0.0, 130.0, 0.0, 20.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Selecione a Refeição",
                  style: TextStyle(
                      fontSize: 28
                  ),
                ),
                Divider(),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: FlatButton(
                    child: Column(
                      children: <Widget>[
                        Image.asset(
                          "imgs/cafe.jpg",
                          fit: BoxFit.cover,
                          height: 100,
                        ),
                        Text("Café da Manhã"),
                      ],
                    ),
                    onPressed: () {
                      passarRefeicao(REFEICAO_CAFE);
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: FlatButton(
                    child: Column(
                      children: <Widget>[
                        Image.asset(
                          "imgs/almoco.png",
                          fit: BoxFit.cover,
                          height: 95,
                        ),
                        Text("Almoço"),
                      ],
                    ),
                    onPressed: () {
                      passarRefeicao(REFEICAO_ALMOCO);
                    }
                ),
              ),

            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: FlatButton(
                    child: Column(
                      children: <Widget>[
                        Image.asset(
                          "imgs/lanche.jpg",
                          fit: BoxFit.cover,
                          height: 100,
                        ),
                        Text("Lanche"),
                      ],
                    ),
                    onPressed: () {
                      passarRefeicao(REFEICAO_LANCHE);
                    }
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(12.0),
                child: FlatButton(
                    child: Column(
                      children: <Widget>[
                        Image.asset(
                          "imgs/janta.png",
                          fit: BoxFit.cover,
                          height: 100,
                        ),
                        Text("Janta"),
                      ],
                    ),
                    onPressed: () {
                      passarRefeicao(REFEICAO_JANTA);
                    }
                ),
              ),

            ],
          ),
        ],
      ),
    );
  }

  void passarRefeicao(String refeicao){
    Future<bool> f = Navigator.push(context, MaterialPageRoute(builder: (context) => CadastrarAlimentacao(refeicao)));
    f.then((cadastrado){
      Navigator.pop(context);
    });
  }
}
