import 'package:flutter/material.dart';

class SucessoPostagem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Center(
            child: Image.asset(
              "imgs/checked.png",
              fit: BoxFit.cover,
              height: 150,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(20.0),
            child: Center(child: Text("Publicado com sucesso!!!",style: TextStyle(fontSize: 22),)),
          ),
        ],
      ),
    );
  }
}
