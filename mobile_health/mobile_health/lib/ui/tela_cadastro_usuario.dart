import 'package:flutter/material.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/models/usuario.dart';
import 'package:mobile_health/service/database.dart';
import 'package:mobile_health/service/sharedpreference.dart';
import 'package:mobile_health/ui/home.dart';
import 'package:mobile_health/ui/loading.dart';
import 'package:toast/toast.dart';


class CadastroUsuario extends StatefulWidget {
  final Usuario usuario;
  final DatabaseService db;

  CadastroUsuario(this.db, this.usuario);

  @override
  _CadastroUsuarioState createState() => _CadastroUsuarioState(db, usuario);
}

class _CadastroUsuarioState extends State<CadastroUsuario> {

  Usuario usuario;
  DatabaseService db;

  _CadastroUsuarioState(this.db, this.usuario);

  String error = "";

  final TextEditingController _nomeControler = TextEditingController();
  final TextEditingController _alturaControler = TextEditingController();
  final TextEditingController _pesoControler = TextEditingController();
  final TextEditingController _usernameControler = TextEditingController();
  final _validator = GlobalKey<FormState>();
  bool loading = false;

  @override
  Widget build(BuildContext context) {
    return loading ? Loading():Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(10.0, 0.0, 10.0, 0.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(42.0, 60.0, 8.0, 0),
              child: Row(
                children: <Widget>[
                  Image.asset(
                    "imgs/logo.jpg",
                    fit: BoxFit.cover,
                    height: 150.0,
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 15,0, 0),
                        child: Text(
                          "Cadastro",
                          style: TextStyle(
                            color: Colors.red,
                            fontSize: 24,
                          ),
                        ),
                      )
                    ],
                  ),
                  Form(
                    key: _validator,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          controller: _nomeControler,
                          validator: (val) =>
                          val.isEmpty
                              ? 'Campo Nome é obrigatório.'
                              : null,
                          decoration: InputDecoration(
                            labelText: "Nome: ",
                            hintText: "Your Full Name",
                          ),
                        ),
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Expanded(
                                // wrap your Column in Expanded
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      0, 15, 8.0, 0),
                                  child: Column(children: <Widget>[
                                    Container(
                                      child: TextFormField(
                                        controller: _alturaControler,
                                        validator: (val) =>
                                        val.isEmpty
                                            ? 'Campo Altura é obrigatório.'
                                            : null,
                                        decoration: InputDecoration(
                                          labelText: "Altura (cm): ",
                                        ),
                                        keyboardType: TextInputType
                                            .numberWithOptions(decimal: true),
                                      ),
                                    ),
                                  ]),
                                ),
                              ),
                              Expanded(
                                // wrap your Column in Expanded
                                child: Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 15, 0, 0),
                                  child: Column(children: <Widget>[
                                    Container(
                                      child: TextFormField(
                                        controller: _pesoControler,
                                        validator: (val) =>
                                        val.isEmpty
                                            ? 'Campo Peso é obrigatório.'
                                            : null,
                                        decoration: InputDecoration(
                                          labelText: "Peso (kg): ",
                                        ),
                                        keyboardType: TextInputType.number,
                                      ),
                                    ),
                                  ]),
                                ),
                              ),
                            ]),
                        TextFormField(
                          controller: _usernameControler,
                          validator: (val) =>
                          val.isEmpty
                              ? 'Campo Usarname é obrigatório.'
                              : null,
                          decoration: InputDecoration(
                            labelText: "Username: ",
                          ),
                        ),
                      ],
                    ),
                  ),
                  Divider(),
                  Container(
                    height: 75,
                    width: 300,
                    child: Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 0),
                        child: FlatButton(
                          color: Colors.red,
                          textColor: Colors.white,
                          disabledColor: Colors.grey,
                          disabledTextColor: Colors.black,
                          padding: EdgeInsets.all(8.0),
                          splashColor: Colors.redAccent,
                          onPressed: () async {

                            if(_validator.currentState.validate()){

                              setState(() {
                                loading = true;
                              });

                              bool existe = await db.usernameJaExiste(
                                  _usernameControler.text);

                              if (!existe) {
                                carregarNovoUsuarioEmObjeto();
                                int id = await db.cadastrarUsuario(usuario);
                                if (id != null) {
                                  List<String> aux = usuario.getNomeUsuario().split(" ");
                                  int tamanho = aux.length - 1;
                                  String nome;
                                  if(tamanho>0){
                                    nome = "${usuario.getNomeUsuario().split(" ")[0]} ${usuario.getNomeUsuario().split(" ")[tamanho]}";
                                  }else{
                                    nome = usuario.getNomeUsuario();
                                  }
                                  addStringToSharedPreference(ID_USUARIO, id.toString());
                                  addStringToSharedPreference(NOME_USUARIO,nome);
                                  addBooleanToSharedPreference(LOGADO, true);

                                  Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));

                                }
                              } else {
                                setState(() {
                                  loading = false;
                                });

                                Toast.show("O username informado ja está em uso.", context, duration: 4, gravity:  Toast.BOTTOM);
                              }

                            }

                          },
                          child: Text(
                            "Entrar",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  carregarNovoUsuarioEmObjeto() {
    usuario.setNomeUsuario(_nomeControler.text);
    usuario.setAltura(int.parse(_alturaControler.text));
    usuario.setPeso(double.parse(_pesoControler.text));
    usuario.setUsername(_usernameControler.text);
  }
}
