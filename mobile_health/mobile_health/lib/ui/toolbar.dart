import 'package:flutter/material.dart';
import 'package:mobile_health/ui/cadastrar_pesagem.dart';
import 'package:mobile_health/ui/comunidade.dart';
import 'package:mobile_health/ui/consulta_alimentaca.dart';
import 'package:mobile_health/ui/consulta_exercicio.dart';



BuildContext _context;

Widget toolbar(BuildContext context) {
  _context = context;

  return BottomNavigationBar(
    type: BottomNavigationBarType.fixed,
    items: const <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.directions_run),
        title: Text("Atividade"),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.accessibility_new),
        title: Text("Peso"),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.fastfood),
        title: Text("Refeicao"),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.share),
        title: Text("Comunidade"),
      ),
    ],
    selectedItemColor: Colors.black,
    unselectedItemColor: Colors.black,
    selectedFontSize: 11,
    onTap: _onItemTapped,

  );
}

void _onItemTapped(int index) {
    switch(index){
      case 0:
        Navigator.push(_context, MaterialPageRoute(builder: (_context) => GraficoGastoCalorico()));
        break;
      case 1:
        Navigator.push(_context, MaterialPageRoute(builder: (_context) => CadastrarPesagem()));
        break;
      case 2:
        Navigator.push(_context, MaterialPageRoute(builder: (_context) => GraficoCalorias()));
        break;
      case 3:
        Navigator.push(_context, MaterialPageRoute(builder: (_context) => Comunidade()));
        break;
    }
}