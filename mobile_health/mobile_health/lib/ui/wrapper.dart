import 'package:flutter/material.dart';
import 'package:mobile_health/constantes/constantes.dart';
import 'package:mobile_health/service/sharedpreference.dart';
import 'package:mobile_health/ui/entrar.dart';
import 'package:mobile_health/ui/home.dart';

class Wrapper extends StatelessWidget {

  bool isLogado;


  Future<bool> estaLogado() async {
    isLogado = await readBooleanSharedPreference(LOGADO);
    return isLogado;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: estaLogado(),
      builder: (context, snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.active:
          case ConnectionState.waiting:
          case ConnectionState.done:
            if (!snapshot.hasError) {
              if (snapshot.data) {
                return Home();
              } else {
                return Entrar();
              }
            }
        }
      },
    );
  }
}
