
class DateUtils {

  static int numeroSemanaAno() {
    DateTime d = DateTime.now();

    final date = d;
    final startOfYear = new DateTime(date.year, 1, 1, 0, 0);
    final firstMonday = startOfYear.weekday;
    final daysInFirstWeek = 8 - firstMonday;
    final diff = date.difference(startOfYear);
    int weeks = ((diff.inDays - daysInFirstWeek) / 7).ceil();
// It might differ how you want to treat the first week
    if (daysInFirstWeek > 3) {
      weeks += 1;
    }
    return weeks;
  }

  static int numeroDiaSemana(){
    DateTime d = DateTime.now();
    return d.weekday;
  }


}